package com.venta.repo;

import com.venta.model.Venta;

public interface IVentaRepo extends IGenericRepo<Venta, Integer>	{

}
