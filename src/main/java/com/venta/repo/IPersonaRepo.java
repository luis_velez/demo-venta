package com.venta.repo;

import com.venta.model.Persona;

public interface IPersonaRepo extends IGenericRepo<Persona, Integer>	{

}
