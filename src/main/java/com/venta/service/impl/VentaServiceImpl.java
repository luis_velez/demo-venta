package com.venta.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.venta.model.Venta;
import com.venta.repo.IVentaRepo;
import com.venta.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService {

	@Autowired
	private IVentaRepo repo;


	@Override
	public Venta crear(Venta venta) throws Exception {
		venta.getDetVenta().forEach(det -> det.setVenta(venta));
		return repo.save(venta);
	}

	@Override
	public Venta listarPorId(Integer id) {
		return repo.findById(id).orElse(null);
	}


}
