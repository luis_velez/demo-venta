package com.venta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoVentaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoVentaApplication.class, args);
	}

}
