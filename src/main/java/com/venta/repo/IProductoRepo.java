package com.venta.repo;

import com.venta.model.Producto;

public interface IProductoRepo extends IGenericRepo<Producto, Integer>	{

}
