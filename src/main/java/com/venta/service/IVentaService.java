package com.venta.service;

import com.venta.model.Venta;

public interface IVentaService {

	Venta listarPorId(Integer id) throws Exception;
	Venta crear(Venta venta) throws Exception;

}
