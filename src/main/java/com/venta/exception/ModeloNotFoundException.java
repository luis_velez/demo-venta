package com.venta.exception;

//@ResponseStatus(HttpStatus.NOT_FOUND)
public class ModeloNotFoundException extends RuntimeException{
	
	private static final long serialVersionUID = 3409301580774055531L;

	public ModeloNotFoundException(String mensaje) {
		super(mensaje);
	}
}
